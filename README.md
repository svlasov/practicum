Practical knowledge (almost) any software development professional (devs, ops, testers etc.) should posses.

Topics
---

 - Linux
 - Source Control
    - git
    - GitHub, GitLab
 - Documenting
    - Markdown
 - Issue Tracking Software
    - JIRA
 - Networking
 - Web
    - how it works
    - Client side
    - Server side
 - Cloud
    - AWS
    - GCP
 - Software/Middleware
    - Web Servers
        - apache2
        - nginx
    - Databases
        - MySQL
        - MongoDB
    - Log analysis
    - Graphing
 - Testing Software
    - Web (Selenium)
 - Programming Languages
    - Shell (bash)
    - Python (2/3)
    - JavaScript (ECMAScript 5, 6+)
    
    